# Flow Diagram

The diagram illustrates the process flow on submission of a t.change.

```mermaid
graph LR

Change_Tracker[Change Tracker]
Gate
Policy_Handler[Policy Handler]
Evidence_Tracker[Evidence Tracker]
tchange((t.change)) -.-> Change_Tracker
Change_Tracker --> Gate

Gate --- Orchestrator
Gate --- Constructor
Gate --- Validator
Gate --- Policy_Handler

Constructor --> Evidence_Tracker
tartefact -.-> Validator
Orchestrator --> Evidence_Tracker
Validator --> Evidence_Tracker
Evidence_Tracker -.-> tevidence((t.evidence))
Change_Tracker --> Evidence_Tracker
Policy_Handler --> Evidence_Tracker

Orchestrator -.-> tenvironment((t.environment))
tenvironment -.-> Validator
tenvironment -.-> Constructor
Constructor -.-> tartefact((t.artefact))
```
