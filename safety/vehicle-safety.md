## Trustable Software for Vehicle Safety

We have identified that safety is one of the key factors for trustable
software. However safety is property of a whole system, not software alone.
So in order to understand how to work towards the safety element of
trustable software, it is logical to consider actual systems. As an initial
exercise we have chosen automotive vehicles as an example for study in order
to gain practical insight.

Automotive vehicles are heavy objects which travel at speed under the control
of imperfect operators (drivers) and in the presence of uncontrolled external
factors (traffic, pedestrians, weather, highways infrastructure). This
scenario is inherently unsafe and accidents occur freqently.

Vehicle-level safety involves the design and implementation of mechanisms to
reduce the frequency and severity of vehicle-related losses for people and
automotive manufacturers.

In order to consider the role of Trustable Software in Vehicle Safety, we are
aiming to apply [STPA](http://sunnyday.mit.edu/STPA-Primer-v0.pdf) which
provides a framework method to establish and record:

* Losses that can affect customers/drivers/passengers/people
* Losses that can affect vehicle manufacturers (including supply chain)
* Hazards which can lead to the losses
* Safety Constraints which provide mitigation/defence against the hazards

Safety Constraints are often described in the STPA literature as 'safety
requirements'. However starting from a Trustable Software perspective it seems
they may be more like what we currently call Intents.

NOTES:

* This work focuses on road vehicles driven by humans. Consideration of
  'autonomous vehicles' is expressly out-of-scope, although some of the
  reasoning may be reusable.

* Modern vehicles are commonly connected to third-party services via wireless
  while driving, and to third-party equipment for diagnostics. As a result
  any safety analysis must include security/vulnerability consideration.

## Losses

* L-1 : Loss of life or injury to people
* L-2 : Loss of or damage to vehicle
* L-3 : Loss of or damage to objects outside the vehicle
* L-4 : Loss of transportation mission
* L-5 : Loss of traffic flow (road blockages etc.)
* L-6 : Loss of customer satisfaction
* L-7 : Environmental impact (e.g. exhaust fumes)
* L-8 : Manufacturer losses due to recalls, fines and other legal penalties

FIXME: can/should we we aim for a 'complete' list? Does it matter?

## Hazards

* H-1 : Vehicle does not maintain safe distance from terrain and other
  obstacles [L-1, L-2, L-3, L-4, L-5, L-6]
* H-2 : Vehicle drives too fast [L-1, L-2, L-3, L-4, L-5, L-6, L-7]
* H-3 : Vehicle brakes excessively [L-1, L-2, L-3, L-4, L-5, L-6, L-7]
* H-4 : Vehicle does not follow traffic flow e.g. jumps red lights, drives on
  wrong side of the road [L-1, L-2, L-3, L-4, L-5, L-6, L-7]
* H-5 : Vehicle is unpredictable to others e.g. no indicators, drives on wrong
  side of road [L-1, L-2, L-3, L-4, L-5, L-6]
* H-6 : Vehicle does not meet legal requirements/standards (L-1, L-2, L-3,
  L-4, L-5, L-6, L-7, L-8)
* H-7 : Vehicle fails to execute driver's controls
* H-8 : Vehicle distracts driver

FIXME: can/should we we aim for a 'complete' list? Does it matter?

## Driver

Inputs

* Sight (environment, warning lights, mirrors)
* Hearing (environment, warning sounds)
* Touch (steering wheel, gearstick, switches)

Outputs

* Feet
* Hands/fingers

FIXME: can/should we we aim for a 'complete' list? Does it matter?

## Controls

These are the things which a driver can use to influence the safety-relevant
behaviour (and the trajectory) of the vehicle.

* Brake pedal (and handbrake if present)
* Steering wheel
* Gear stick (and clutch if present)
* Switches for headlights, sidelights, fog lights
* Switches for turn indicators
* Switch for hazard indicator
* Switch for wiper control

FIXME: can/should we we aim for a 'complete' list? Does it matter?

FIXME: there are other controls - operating them may directly or indirectly
lead to H-8. How does that get handled via STPA?

## Driver Assistance (including feedback mechanisms)

* Resistance from steering wheel, pedals, gear stick
* Mirrors
* Cameras
* Feedback from switches (haptics)
* Windscreen wipers
* Traction control
* ABS
* Proximity sensors (at speed and/or parking)
* Lane sensors

FIXME: can/should we aim for a 'complete' list? Does it matter?

