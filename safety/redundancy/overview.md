# Techniques for improving reliability to support safety objectives

## Redundancy

Redundant architectectures are routinely considered for achieving higher levels of risk reduction that might otherwise be impossible or more costly to achieve using single devices or systems.

Redundancy can help achieve system risk goals with devices that by themselves cannot be trusted for one or more of the following reasons:
- failure modes of the constituent components are not well defined; or
- behaviour of the subsystem under fault conditions cannot be completely determined; or
- there is insufficient dependable field experience to support a claimed failure rate for detected and undetected dangerous failures

### Proven in use

Dependable field data comes with some significant expectations including:

Expectation | Detail
--- | ---
Time in use | 1 or 2 years with no signficant changes
Hours in use | 30,000,000 hours and 10 different applications
Operating conditions | Stress conditions equal to or above average
Failure rate calculation | Confidence limits and conservative assumptions about returns
Failure data comparison | Dangerous undetected failures are less than 20%
Basic quality system | Control of manufacturing operations and field failures, also sufficient configuration management and change control

In general redundancy adds cost since we must provide some duplication of the primary functions and often it is desirable to add diagnostic features that monitor for, or compare, behaviours of more than one system.

For more details on the mathematical basis of these expectations see reference 1 below.

# Architectures - Notation summary

In reliability engineering, redundant designs are described using a shorthand "k out-of k" (oo) notation. This applies to the inputs, control elements or actuators in a system and is used to summarize the fault tolerance of a system. Hardware fault tolerance is a measure of whether a system can continue to perform the function when one or more dangerous faults are present.

KooN - K element failures out of N elements total needed to work
KooND - K faulures out of N with Diagnostics

## 1oo1

A 1oo1 design means the function cannot be performed if even a single fault occurs. If the function is related to risk reduction then a single dangerous fault means the safety function cannot be performed.

```mermaid
graph LR
  subgraph 1oo1
  START[ ]-->A[Channel]
  A-->STOP[ ]
  end

  style START fill:#eaf2fb, stroke:#eaf2fb
  style STOP fill:#eaf2fb, stroke:#eaf2fb
 ```

## 1oo1D

The presence of diagnostics can reduce the contribution of dangerous undetected failures to the overall failure rate. This increases the safe failure fraction (see 2 below)

```mermaid
graph LR
  subgraph 1oo1D
  START[ ]-->A[Channel]
  A-->C((1oo1 D))
  A-->B[Diagnostics]
  B-.->C
  C-->STOP[ ]
  end

  style START fill:#eaf2fb, stroke:#eaf2fb
  style STOP fill:#eaf2fb, stroke:#eaf2fb
 ```

The probability of failure on demand can be modeled using a fault tree as follows:

```mermaid
graph TD
  VOSG[1oo1 PFD]-->OR{Or}
  OR-->PIF
  OR-->DD
  OR-->DUTI((DU TI))
  OR-->DUMT((DU MT))
  OR-->PTD((PTD))
```

Similarly for this type of architecture we can model the fail safely opportunities.
These safe faults might be false trips. Unwanted stoppages that reduce
availability but don't introduce a dangerous condition or outcome.

```mermaid
graph TD
  VOSG[1oo1 PFS]-->OR{Or}
  OR-->SD
  OR-->SU
```

The types of failure are summarized in the table below:

Acronym | Failure type
--- | ---
VOSG | Failure to achieve what is needed for safety - (V)iolation (O)f (S)afety (G)oal
PFD, PFS | (P)robability of (F)ailure on (Demand) or (P)robability of (F)ail (S)afely
OR | Boolean OR operator - any one failure will propagate
PIF | (P)robability of (I)nitial (F)ailure
SD, DD | Safe Detected, Dangerous Detected
SU, DU MT, DU TI | Safe Undetected, Dangerous Undetected (during Mission Time & during Test Interval)
PTD | Proof Test Duration

All control systems have a statistical probability of exhibiting these classes
of fault. Determining the individual failures and their probability contribution
to the overall failure rate is the domain of hardware reliability engineering.

## 1oo2D

In automotive electronics malfunctions are expected to be assessed and managed
and a typical approach is to implement a safety mechanism that can protect or
detect some or all of the dangerous failures of a control system. 1oo2 systems
can be used to implement a safety mechanism that provides some coverage of the
faults of the underlying control channel. Care is take to identify and limit the
opportunity for shared weaknesses between the channels to arise at the same time.
This type of common cause failure reduces the overall reliability causing both
the control channel and safety mechanism to fail at the same time due to the
same initiating event.

The physical block diagram of a 1oo2D suitable for high safety and availability
but not suited to strict fail operation is shown below:

![1oo2d Physical Block Diagram](1oo2D Physical.png)

When one channel detects a failure (either with itself or with the redundant
channel) the comparator can continue operation with the working channel. If the
comparator detects a mismatch then without knowing which channel is at fault the
only action available may be to bring the outputs to a safe state. The fault
trees show the increased failure rate due to shared initiating events (common
cause failure) and the additional opportunity for false trips when compared to
the 1oo1 architecture.

```mermaid
graph TD
  VOSG[1oo2 PFD]-->OR1{Or}
  OR1-->CC1[Common Cause PDF]
  CC1-->OR2{Or}
  OR1-->AND1{And}
  AND1-->DA[Device A PFD]
  AND1-->DB[Device B PFD]
  OR2-->PIF1
  OR2-->DD1
  OR2-->DUTI1((DU TI))
  OR2-->DUMT1((DU MT))
  DA-->OR3{Or}
  OR3-->PIF2
  OR3-->DD2
  OR3-->DUTI2((DU TI))
  OR3-->DUMT2((DU MT))
  OR3-->PTD3((PTD))
  DB-->OR4{Or}
  OR4-->PIF3
  OR4-->DD3
  OR4-->DUTI3((DU TI))
  OR4-->DUMT3((DU MT))
  OR4-->PTD3((PTD))
```

```mermaid
graph TD
  VOSG[1oo2 PFS]-->OR{Or}
  OR-->SD1[SD Common cause]
  OR-->SU1[SU Common cause]
  OR-->SDA[SD A]
  OR-->SUA[SD A]
  OR-->SDB[SU B]
  OR-->SUB[SU B]
```

## 2002D


## 2004D


## References

1 [Proven in use](https://www.exida.com/articles/What-Does-Proven-In-Use-Imply.pdf "What does proven in use imply")

2 [Safe Failure Fraction](https://www.exida.com/images/uploads/exida_Position_on_IEC_61508_2010_definitions_minimum_HFT_v4.pdf "Definitions regarding minimum hardware fault tolerance")