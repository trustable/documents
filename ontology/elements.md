# Elements

## Introduction

This document defines a set of terms and their defined relationships and essential features, which can be used to describe the creation and refinement of a body of work. Real-world processes and practices can be mapped onto this model to facilitate analysis and comparison.

These are not limited to software, but may form the basis of a separate set of [concepts](concepts.md) for modelling the practice of software development and the consumption of that software by systems.

The following definitions apply throughout this document:
* **Elements** are representations of the body of work or information relating to it
* **Characteristics** are the attributes of a given class of Elements
* **Properties** are the attributes of a specific Element
* **Actions** are operations that impact specific Elements

Only the Actions, Elements and their defining Characteristics are defined in this document. Future work will aim to define a common set of Properties that might be expected to be available for specific instances of these Elements, and to devise a common notation to describe stored evidence of these that could be consumed by tooling. An organisation may then choose whether to preserve a copy of this extracted evidence over time or forensically evaluate the tooling and data *in situ* at time of analysis, but may do so using a common frame of reference.

The keywords MAY, SHOULD and MUST in this document are to be interpreted as described in [RFC2119](https://www.ietf.org/rfc/rfc2119.txt).

## Overview

![Trustable concepts](trustable-elements.png)

The set of Elements is as follows:
* **t.identity**: a representation of the entity associated with an Action
* **t.proposal**: a formal suggestion of changes or additions to the body of work
* **t.response**: information recorded by a t.identity about a specific t.proposal
* **t.history**: a recorded sequence of evaluated t.proposals
* **t.marker**: an identifier for a particular aggregation of evaluated t.proposals in a t.history
* **t.artifact**: a discrete part or constituent of a t.proposal

The set of Actions is as follows:
* **SUBMIT** - A t.identity makes a t.proposal available for evaluation
* **RECORD** - A t.identity records a t.response about a t.proposal
* **EVALUATE** - A t.identity considers a t.proposal for inclusion in a t.history
* **ASSERT** - A t.identity attaches a t.marker to a t.history
* **CONSUME** - A t.identity obtains a set of t.artifacts from a t.history

Using these terms, the creation and refinement of a body of work can be modelled as follows:

* A *t.identity* *SUBMITS* a *t.proposal* consisting of a set of *t.artifacts*.
* A (possibly empty) set of *t.identities* *RECORDS* *t.responses* about the *t.proposal*.
* The *t.proposal* (together with its associated *t.responses*) is *EVALUATED* against a particular *t.history* of previous *t.proposals*, which may result in its inclusion in that *t.history*.
* After inclusion, a *t.identity* may *ASSERT* a *t.marker* to any *t.proposal* in the *t.history*, to indicate that a particular point in history was assigned to some use, or had some property not known at the time of evaluation.
* A *t.history* may be *CONSUMED* by a *t.identity* to perform some action (build, install, whatever) on or with the constituent *t.artifacts*, or possibly the metadata of the *t.history* itself.

## t.identity
A t.identity is a representation of an entity associated with an Action.

The corresponding entity may be a person or a computer system, an organisation or group of such entities, or a formal legal entity, such as a corporation.

**Characteristics**:
* A given entity MAY be associated with more than one t.identity
* A given t.identity MAY be used by more than one entity

## t.proposal
A t.proposal is a formal suggestion of changes or additions to the body of work.

A t.proposal is subject to evaluation, which may be informed by and/or recorded in *t.responses*, before it can be aggregated with *t.history*.

**Characteristics**:
* A t.proposal MAY be applied to a t.history as part of an *Evaluate* action
* A t.proposal is created or modified by a *Submit* action

## t.response
A t.response is an opinion or the result of an evaluative process recorded by a t.identity regarding a specific t.proposal.

**Characteristics**:
* A t.response is created or modified by a *Record* action
* A t.response MAY inform or relate to an *Evaluate* action

## t.marker
A t.marker is an identifier for a particular aggregation of evaluated *t.proposals* in a *t.history*.

Its refers to a specific historical version of the body of work and may also include additional information relating to the body of work at that point in its history.

**Characteristics**:
* A t.marker is created or modified by an *Assert* action
* A t.marker MAY inform or relate to a *Consume* action
* A t.marker MAY include additional information

## t.history
A t.history is a recorded sequence of evaluated *t.proposals*.

It represents an accumulated sequence of additions or revisions to a body of work.

**Characteristics**:
* A t.history MAY be created or modified by an *Evaluate* action

## t.artifact
A t.artifact is a discrete part or constituent of a *t.proposal*.

It may be anything submitted as part of a *t.proposal*, whether created by a human or as a result of preparative or investigative procedures. The complete set of t.artifacts represents the body of work under consideration.

**Characteristics**:
* A t.artifact MUST be represented as part of a *t.history* or a *t.proposal*
