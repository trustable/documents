# README

The [trustable/documents](https://gitlab.com/trustable/documents) project aims to create a methodology that helps to answer the question:

*"What makes software Trustable?"*

The Trustable Hypothesis, which was shared in the original ['call to arms'](https://lists.trustable.io/pipermail/trustable-software/2016-July/000000.html) on the Trustable mailing list, proposes a set of characteristics for Trustable Software:

* Provenance (*We know where it comes from*)
* Construction (*We know how to build it and we can reproduce it*)
* Maintenance (*We can update it and be confident that it will not break or regress*)
* Function (*It does what it is supposed to do and it does not do what it is not supposed to do*)
* Protection (*We have some confidence that it won't harm our communities and our children*)

The [overall goal](goals.md) of this project is to create a methodology for analysing and evaluating software engineering practices, which allows us to:
* Investigate claims made about software on the basis of those practices
* Assess the extent to which the resultant software may be considered 'trustable', and
* Compare different processes and practices using a common frame of reference.

To address the *Protection* factor, the project repository includes material relating to specific types of harm or loss that a system may protect against, under the headings of [Safety](safety/approach.md) and [Security](security.approach.md); material relating to other protective categories may be added as the project continues.

Note:
* The *practice* and *requirements* directories contain some earlier work examining examples of 'good' practice and investigating approaches for capturing requirements. While these should be considered deprecated as a result of recent work on the [ontology](elements.md), they have been retained for future reference.
* The *deprecated* directory contains historical material that has been explicitly superseded by the ontology; this material will be removed in a future update.

## Licence

All documentation for the [Trustable software project](https://trustable.io) is licensed as CC0 by default.

![CC Zero](http://i.creativecommons.org/p/zero/1.0/88x31.png)
